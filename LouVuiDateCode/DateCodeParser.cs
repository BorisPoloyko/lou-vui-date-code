using System;

namespace LouVuiDateCode
{
    public static class DateCodeParser
    {
        /// <summary>
        /// Parses a date code and returns a <see cref="manufacturingYear"/> and <see cref="manufacturingMonth"/>.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void ParseEarly1980Code(string dateCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length < 3 || dateCode.Length > 4)
            {
                throw new ArgumentException("Wrong code format!");
            }

            foreach (char i in dateCode)
            {
                if (char.IsLetter(i))
                {
                    throw new ArgumentException("Wrong code format!");
                }
            }

            manufacturingYear = 1900 + ((uint)(dateCode[0] - '0') * 10) + (uint)(dateCode[1] - '0');
            if (dateCode.Length == 3)
            {
                manufacturingMonth = (uint)(dateCode[2] - '0');
            }
            else
            {
                manufacturingMonth = ((uint)(dateCode[2] - '0') * 10) + (uint)(dateCode[3] - '0');
            }
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingMonth"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void ParseLate1980Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length < 5 || dateCode.Length > 6)
            {
                throw new ArgumentException("Wrong code format!");
            }

            factoryLocationCode = dateCode.Substring(dateCode.Length - 2, 2);
            factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
            manufacturingYear = 1900 + ((uint)(dateCode[0] - '0') * 10) + (uint)(dateCode[1] - '0');
            string month = dateCode.Substring(2, dateCode.Length - 4);
            if (month.Length == 1)
            {
                manufacturingMonth = (uint)month[0] - '0';
            }
            else
            {
                manufacturingMonth = (((uint)(month[0] - '0')) * 10) + (uint)(month[1] - '0');
            }
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingMonth"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingMonth">A manufacturing month to return.</param>
        public static void Parse1990Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException("Wrong code format!");
            }

            factoryLocationCode = dateCode.Substring(0, 2);
            factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);
            manufacturingMonth = ((uint)(dateCode[2] - '0') * 10) + (uint)(dateCode[4] - '0');
            if (dateCode[3] - '0' != 9)
            {
                manufacturingYear = 2000 + ((uint)(dateCode[3] - '0') * 10) + (uint)(dateCode[^1] - '0');
            }
            else
            {
                manufacturingYear = 1990 + (uint)(dateCode[^1] - '0');
            }
        }

        /// <summary>
        /// Parses a date code and returns a <paramref name="factoryLocationCode"/>, <paramref name="manufacturingYear"/>, <paramref name="manufacturingWeek"/> and <paramref name="factoryLocationCountry"/> array.
        /// </summary>
        /// <param name="dateCode">A three or four number date code.</param>
        /// <param name="factoryLocationCountry">A factory location country array.</param>
        /// <param name="factoryLocationCode">A factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year to return.</param>
        /// <param name="manufacturingWeek">A manufacturing month to return.</param>
        public static void Parse2007Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingWeek)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException("Wrong code format!");
            }

            factoryLocationCode = dateCode.Substring(0, 2);
            factoryLocationCountry = CountryParser.GetCountry(factoryLocationCode);

            manufacturingWeek = ((uint)(dateCode[2] - '0') * 10) + (uint)(dateCode[4] - '0');
            manufacturingYear = 2000 + ((uint)(dateCode[3] - '0') * 10) + (uint)(dateCode[^1] - '0');
        }
    }
}
