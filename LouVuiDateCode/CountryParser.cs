using System;
using System.Globalization;

namespace LouVuiDateCode
{
    public static class CountryParser
    {
        /// <summary>
        /// Gets a an array of <see cref="Country"/> enumeration values for a specified factory location code. One location code can belong to many countries.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <returns>An array of <see cref="Country"/> enumeration values.</returns>
        public static Country[] GetCountry(string factoryLocationCode)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            char[] separators = { ' ', ',' };
            string[] franceCodes = "A0, A1, A2, AA, AAS, AH, AN, AR, AS, BA, BJ, BU, DR, DU, DR, DT, CO, CT, CX, ET, FL, LW, MB, MI, NO, RA, RI, SD, SF, SL, SN, SP, SR, TJ, TH, TR, TS, VI, VX".Split(separators);
            string[] germanyCodes = "LP, OL".Split(separators);
            string[] italyCodes = "BC, BO, CE, FO, MA, OB, RC, RE, SA, TD".Split(separators);
            string[] spainCodes = "CA, LO, LB, LM, LW, GI".Split(separators);
            string[] swissCodes = "DI, FA".Split(separators);
            string[] usaCodes = "FC, FH, LA, OS, SD, FL".Split(separators);

            if (factoryLocationCode.ToUpper(CultureInfo.CurrentCulture) == "FL")
            {
                return new[] { Country.France, Country.USA };
            }

            if (factoryLocationCode.ToUpper(CultureInfo.CurrentCulture) == "LW")
            {
                return new[] { Country.France, Country.Spain };
            }

            if (factoryLocationCode.ToUpper(CultureInfo.CurrentCulture) == "SD")
            {
                return new[] { Country.France, Country.USA };
            }

            foreach (string code in franceCodes)
            {
                if (code == factoryLocationCode.ToUpper(CultureInfo.CurrentCulture))
                {
                    return new[] { Country.France };
                }
            }

            foreach (string code in germanyCodes)
            {
                if (code == factoryLocationCode.ToUpper(CultureInfo.CurrentCulture))
                {
                    return new[] { Country.Germany };
                }
            }

            foreach (string code in italyCodes)
            {
                if (code == factoryLocationCode.ToUpper(CultureInfo.CurrentCulture))
                {
                    return new[] { Country.Italy };
                }
            }

            foreach (string code in spainCodes)
            {
                if (code == factoryLocationCode.ToUpper(CultureInfo.CurrentCulture))
                {
                    return new[] { Country.Spain };
                }
            }

            foreach (string code in swissCodes)
            {
                if (code == factoryLocationCode.ToUpper(CultureInfo.CurrentCulture))
                {
                    return new[] { Country.Switzerland };
                }
            }

            foreach (string code in usaCodes)
            {
                if (code == factoryLocationCode.ToUpper(CultureInfo.CurrentCulture))
                {
                    return new[] { Country.USA };
                }
            }

            return Array.Empty<Country>();
        }
    }
}
