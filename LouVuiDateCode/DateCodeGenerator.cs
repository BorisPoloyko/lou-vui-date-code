using System;
using System.Globalization;

namespace LouVuiDateCode
{
    public static class DateCodeGenerator
    {
        /// <summary>
        /// Generates a date code using rules from early 1980s.
        /// </summary>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateEarly1980Code(uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingYear < 1980 || manufacturingYear > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth < 1 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            return (manufacturingYear % 1900).ToString(CultureInfo.CurrentCulture) + manufacturingMonth;
        }

        /// <summary>
        /// Generates a date code using rules from early 1980s.
        /// </summary>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateEarly1980Code(DateTime manufacturingDate)
        {
            if (manufacturingDate.Year < 1980 || manufacturingDate.Year > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (manufacturingDate.Month < 1 || manufacturingDate.Month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            return (manufacturingDate.Year % 1900).ToString(CultureInfo.CurrentCulture) + manufacturingDate.Month;
        }

        /// <summary>
        /// Generates a date code using rules from early 1980s.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateLate1980Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingYear < 1980 || manufacturingYear > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth < 1 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            foreach (char i in factoryLocationCode)
            {
                if (char.IsDigit(i))
                {
                    throw new ArgumentException("Invalid factory code!");
                }
            }

            return (manufacturingYear % 1900).ToString(CultureInfo.CurrentCulture) + manufacturingMonth + factoryLocationCode.ToUpper(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Generates a date code using rules from late 1980s.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string GenerateLate1980Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate.Year < 1980 || manufacturingDate.Year > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (manufacturingDate.Month < 1 || manufacturingDate.Month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            foreach (char i in factoryLocationCode)
            {
                if (char.IsDigit(i))
                {
                    throw new ArgumentException("Invalid factory code!");
                }
            }

            return (manufacturingDate.Year % 1900).ToString(CultureInfo.CurrentCulture) + manufacturingDate.Month + factoryLocationCode.ToUpper(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Generates a date code using rules from 1990 to 2006 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingMonth">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate1990Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingYear < 1990 || manufacturingYear > 2006)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth < 1 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            foreach (char i in factoryLocationCode)
            {
                if (char.IsDigit(i))
                {
                    throw new ArgumentException("Invalid factory code!");
                }
            }

            return factoryLocationCode.ToUpper(CultureInfo.CurrentCulture) + (manufacturingMonth / 10) +
                   ((manufacturingYear % 100) / 10) + (manufacturingMonth % 10) + (manufacturingYear % 10);
        }

        /// <summary>
        /// Generates a date code using rules from 1990 to 2006 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate1990Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate.Year < 1990 || manufacturingDate.Year > 2006)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (manufacturingDate.Month < 1 || manufacturingDate.Month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            foreach (char i in factoryLocationCode)
            {
                if (char.IsDigit(i))
                {
                    throw new ArgumentException("Invalid factory code!");
                }
            }

            return factoryLocationCode.ToUpper(CultureInfo.CurrentCulture) + (manufacturingDate.Month / 10) +
                   ((manufacturingDate.Year % 100) / 10) + (manufacturingDate.Month % 10) + (manufacturingDate.Year % 10);
        }

        /// <summary>
        /// Generates a date code using rules from post 2007 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingYear">A manufacturing year.</param>
        /// <param name="manufacturingWeek">A manufacturing week number.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate2007Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingWeek)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingYear < 2007)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingWeek < 1 || manufacturingWeek > 53)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingWeek));
            }

            foreach (char i in factoryLocationCode)
            {
                if (char.IsDigit(i))
                {
                    throw new ArgumentException("Invalid factory code!");
                }
            }

            return factoryLocationCode.ToUpper(CultureInfo.CurrentCulture) + (manufacturingWeek / 10) +
                   ((manufacturingYear % 100) / 10) + (manufacturingWeek % 10) + (manufacturingYear % 10);
        }

        /// <summary>
        /// Generates a date code using rules from post 2007 period.
        /// </summary>
        /// <param name="factoryLocationCode">A two-letter factory location code.</param>
        /// <param name="manufacturingDate">A manufacturing date.</param>
        /// <returns>A generated date code.</returns>
        public static string Generate2007Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate.Year < 2007)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            CultureInfo ci = CultureInfo.CurrentCulture;

            int weeks = ci.Calendar.GetWeekOfYear(manufacturingDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            if (weeks < 1 || weeks > 53)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            foreach (char i in factoryLocationCode)
            {
                if (char.IsDigit(i))
                {
                    throw new ArgumentException("Invalid factory code!");
                }
            }

            return factoryLocationCode.ToUpper(CultureInfo.CurrentCulture) + (weeks / 10) +
                   ((manufacturingDate.Year % 100) / 10) + (weeks % 10) + (manufacturingDate.Year % 10);
        }
    }
}
